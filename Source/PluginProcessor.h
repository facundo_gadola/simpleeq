#pragma once

#include <JuceHeader.h>
#include "Utils/Fifo.h"


//==============================================================================
// Aliases
using Filter    = juce::dsp::IIR::Filter <float>;
using CutFilter = juce::dsp::ProcessorChain <Filter, Filter, Filter, Filter>;
using MonoChain = juce::dsp::ProcessorChain <CutFilter, Filter, CutFilter>;
using Coefficients = Filter::CoefficientsPtr;


/*
  Slope configuration for low cut and high cut filters.
  The slope defines how steep the “cliff” is at the cutoff point.
 */
enum Slope {
    Slope_12,
    Slope_24,
    Slope_36,
    Slope_48
};

// Struct for storing the parameters values.
struct ChainSettings
{
    float peakFreq {0.f};
    float peakGainInDecibel {0.f};
    float peakQ {1.f};
    
    float lowCutFreq {0};
    Slope lowCutSlope {Slope::Slope_12};
    
    float highCutFreq {0};
    Slope highCutSlope {Slope::Slope_12};
};

// This function returns the parameters values (stored in the value tree state) as ChainSettings
ChainSettings getChainSettings (juce::AudioProcessorValueTreeState & apvts);


// This is used as an index to access the differents filters in the MonoChain (ProcessorChain)
enum ChainPositions {
    LowCut,
    Peak,
    HighCut
};


//==============================================================================

void updateCoefficients (Coefficients& old, const Coefficients& replacements);

template <int Index, typename ChainType, typename CoefficientType>
void update (ChainType& chain, const CoefficientType& coefficients);

template<typename ChainType, typename CoefficientType>
void updateCutFilter (ChainType& chain,
                      const CoefficientType& cutCoefficients,
                      const Slope& slope);


//==============================================================================
// Functions for creating filters
inline Coefficients makePeakFilter (const ChainSettings& chainSettings, double sampleRate)
{
    return juce::dsp::IIR::Coefficients<float>::makePeakFilter(sampleRate,chainSettings.peakFreq, chainSettings.peakQ , juce::Decibels::decibelsToGain(chainSettings.peakGainInDecibel));
}

inline auto makeLowCutFilter (const ChainSettings& chainSettings, double sampleRate)
{
    return juce::dsp::FilterDesign<float>::designIIRHighpassHighOrderButterworthMethod(chainSettings.lowCutFreq, sampleRate , 2 * (chainSettings.lowCutSlope + 1));
}


inline auto makeHighCutFilter (const ChainSettings& chainSettings, double sampleRate)
{
    return juce::dsp::FilterDesign<float>::designIIRLowpassHighOrderButterworthMethod(chainSettings.highCutFreq,sampleRate , 2 * (chainSettings.highCutSlope + 1));
}



//==============================================================================
class SimpleEQAudioProcessor  : public juce::AudioProcessor
{
public:
    //==============================================================================
    SimpleEQAudioProcessor();
    ~SimpleEQAudioProcessor() override;

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (juce::AudioBuffer<float>&, juce::MidiBuffer&) override;

    //==============================================================================
    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const juce::String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const juce::String getProgramName (int index) override;
    void changeProgramName (int index, const juce::String& newName) override;

    //==============================================================================
    void getStateInformation (juce::MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
    
    //==============================================================================


    juce::AudioProcessorValueTreeState::ParameterLayout createParameterLayout();
    juce::AudioProcessorValueTreeState apvts;
    
    
    // This is for the spectrum analyzer
    SingleChannelSampleFifo<juce::AudioBuffer<float>> leftChannelFifo  {Channel::Left};
    SingleChannelSampleFifo<juce::AudioBuffer<float>> rightChannelFifo {Channel::Right};

    

private:
    
    // 2 MonoChains for stereo
    MonoChain leftChain, rightChain;
    
    //==============================================================================
    // update functions
    void updatePeakFilter     (const ChainSettings& chainSettings);
    void updateLowCutFilters  (const ChainSettings& chainSettings);
    void updateHighCutFilters (const ChainSettings& chainSettings);
    void updateFilters ();
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SimpleEQAudioProcessor)
};
