#pragma once

#include <JuceHeader.h>
#include "FFT_data_generator.h"
#include "Fifo.h"


//==============================================================================
class  PathProducer
{
    
public:
    
    PathProducer(SingleChannelSampleFifo<juce::AudioBuffer<float>>& singleChannelSampleFifo);
    
    void process(juce::Rectangle<float>fftBounds, double sampleRate);
    juce::Path getPath () {return leftChannelFFTPath;}
    
private:
    
    SingleChannelSampleFifo<juce::AudioBuffer<float>>* leftChannelFifo;

    juce::AudioBuffer<float> monoBuffer;

    FFTDataGenerator<std::vector<float>> leftChannelFFTDataGenerator;

    AnalyzerPathGenerator<juce::Path> pathProducer;

    juce::Path leftChannelFFTPath;
    
};
