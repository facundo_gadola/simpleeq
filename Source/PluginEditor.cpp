#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
SimpleEQAudioProcessorEditor::SimpleEQAudioProcessorEditor (SimpleEQAudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p)
{
    
    // labels for each filter (and where to put them)
    // position 0.f --> below the minimum value of the slider
    // position 1.f --> below the maximum value of the slider
    peakFreqSlider.labels.add({0.f, "20Hz"});
    peakFreqSlider.labels.add({1.f, "20kHz"});
    
    peakGainSlider.labels.add({0.f, "-24dB"});
    peakGainSlider.labels.add({1.f, "24dB"});
    
    peakQSlider.labels.add({0.f, "0.1"});
    peakQSlider.labels.add({1.f, "10.0"});
    
    lowCutFreqSlider.labels.add({0.f, "20Hz"});
    lowCutFreqSlider.labels.add({1.f, "20kHz"});
    
    highCutFreqSlider.labels.add({0.f, "20Hz"});
    highCutFreqSlider.labels.add({1.f, "20kHz"});
    
    lowCutSlopeSlider.labels.add({0.f, "12 dB/Oct"});
    lowCutSlopeSlider.labels.add({1.f, "48 dB/Oct"});
    
    highCutSlopeSlider.labels.add({0.f, "12 dB/Oct"});
    highCutSlopeSlider.labels.add({1.f, "48 dB/Oct"});
    
    
    
    for (auto* component: getComponents()  )
    {
        addAndMakeVisible(component);
    }
    
    setSize (650, 550);
}

SimpleEQAudioProcessorEditor::~SimpleEQAudioProcessorEditor()
{

}

//==============================================================================
void SimpleEQAudioProcessorEditor::paint (juce::Graphics& g)
{

    g.fillAll (juce::Colour(55u,58u,102u));
    
    
    g.setColour(juce::Colours::black);

    // draw the rectangles where the names of the filters are
    g.drawRect(lowCutLabelsArea);
    g.drawRect(peakLabelsArea);
    g.drawRect(highCutLabelsArea);
    
    // name of each filter
    g.setColour(juce::Colours::lightgrey);
    g.drawText("Low cut", lowCutLabelsArea,  juce::Justification::centred);
    g.drawText("Peak",    peakLabelsArea,    juce::Justification::centred);
    g.drawText("High cut",highCutLabelsArea, juce::Justification::centred);

}

void SimpleEQAudioProcessorEditor::resized()
{
    auto bounds = getLocalBounds();
    
    auto responseArea = bounds.removeFromTop(bounds.getHeight() * 0.4f);
    responseCurveComponent.setBounds(responseArea);
    
    highCutLabelsArea = bounds.removeFromTop(20);
    lowCutLabelsArea  = highCutLabelsArea.removeFromLeft(highCutLabelsArea.getWidth() / 3);
    peakLabelsArea    = highCutLabelsArea.removeFromLeft(highCutLabelsArea.getWidth() / 2);
    
    auto lowCutArea  = bounds.removeFromLeft(bounds.getWidth() / 3);
    auto highCutArea = bounds.removeFromRight(bounds.getWidth() / 2);
    
    lowCutFreqSlider.setBounds(lowCutArea.removeFromTop(lowCutArea.getHeight() / 2));
    lowCutSlopeSlider.setBounds(lowCutArea);
        
    highCutFreqSlider.setBounds(highCutArea.removeFromTop(highCutArea.getHeight() * 0.5));
    highCutSlopeSlider.setBounds(highCutArea);
    
    peakFreqSlider.setBounds(bounds.removeFromTop(bounds.getHeight() * 0.33));
    peakGainSlider.setBounds(bounds.removeFromTop(bounds.getHeight() * 0.5));
    peakQSlider.setBounds(bounds);
    
    
}



// This function returns a vector of all the GUI components.
std::vector<juce::Component*> SimpleEQAudioProcessorEditor::getComponents()
{
    return {
        &peakFreqSlider,
        &peakGainSlider,
        &peakQSlider,
        &lowCutFreqSlider,
        &highCutFreqSlider,
        &lowCutSlopeSlider,
        &highCutSlopeSlider,
        &responseCurveComponent
    };
}

