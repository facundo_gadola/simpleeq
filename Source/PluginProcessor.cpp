
#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
SimpleEQAudioProcessor::SimpleEQAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       ), apvts(*this, nullptr, juce::Identifier("Parameters"), createParameterLayout())
#endif
{
}

SimpleEQAudioProcessor::~SimpleEQAudioProcessor()
{
}

//==============================================================================
const juce::String SimpleEQAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool SimpleEQAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool SimpleEQAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool SimpleEQAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double SimpleEQAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int SimpleEQAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int SimpleEQAudioProcessor::getCurrentProgram()
{
    return 0;
}

void SimpleEQAudioProcessor::setCurrentProgram (int index)
{
}

const juce::String SimpleEQAudioProcessor::getProgramName (int index)
{
    return {};
}

void SimpleEQAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void SimpleEQAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    juce::dsp::ProcessSpec spec;
    spec.maximumBlockSize = samplesPerBlock;
    spec.numChannels = 1;
    spec.sampleRate = sampleRate;
    
    //prepare both chains
    leftChain.prepare(spec);
    rightChain.prepare(spec);
    
    // update low cut, high cut and peak filters using the
    // parameters values (from chain settings)
    updateFilters();
    
    
    // prepare the fifos. This is for the spectrum analyzer
    leftChannelFifo.prepare(samplesPerBlock);
    rightChannelFifo.prepare(samplesPerBlock);

    
}

void SimpleEQAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool SimpleEQAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void SimpleEQAudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();
    
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());
    
    
    // update low cut, high cut and peak filters using the
    // parameters values (from chain settings)
    updateFilters();
    
    
    juce::dsp::AudioBlock<float> block (buffer);
    
    // extract channels from block
    auto leftBlock  = block.getSingleChannelBlock(0);
    auto rightBlock = block.getSingleChannelBlock(1);
    
    // create context for both channels
    juce::dsp::ProcessContextReplacing<float> leftContext (leftBlock);
    juce::dsp::ProcessContextReplacing<float> rightContext (rightBlock);
    
    // passing context for processing
    leftChain.process(leftContext);
    rightChain.process(rightContext);
    
    
    // update fifos
    leftChannelFifo.update(buffer);
    rightChannelFifo.update(buffer);

}

//==============================================================================
bool SimpleEQAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* SimpleEQAudioProcessor::createEditor()
{
    return new SimpleEQAudioProcessorEditor (*this);
}

//==============================================================================
void SimpleEQAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    juce::MemoryOutputStream memoryOutputStream (destData, true);
    
    // store the parameters values on memory
    apvts.state.writeToStream(memoryOutputStream);
    
}

void SimpleEQAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // loads the state tree from memory
    auto tree = juce::ValueTree::readFromData(data, sizeInBytes);
    if (tree.isValid())
    {
        apvts.replaceState(tree);
        
        // update filters with the new (stored) state
        updateFilters();
    }
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new SimpleEQAudioProcessor();
}


//==============================================================================
// This function returns the parameters values (from the value tree state) as a ChainSettings struct
ChainSettings getChainSettings (juce::AudioProcessorValueTreeState & apvts)
{
    ChainSettings settings;
    
    settings.peakFreq = apvts.getRawParameterValue("Peak Freq")->load();
    settings.peakGainInDecibel = apvts.getRawParameterValue("Peak Gain")->load();
    settings.peakQ = apvts.getRawParameterValue("Peak Q")->load();
    
    settings.lowCutFreq = apvts.getRawParameterValue("LowCut Freq")->load();
    settings.lowCutSlope = static_cast<Slope> (apvts.getRawParameterValue("LowCut Slope")->load());
    
    settings.highCutFreq = apvts.getRawParameterValue("HighCut Freq")->load();
    settings.highCutSlope = static_cast<Slope> (apvts.getRawParameterValue("HighCut Slope")->load());
    
    
    return settings;
}


void updateCoefficients(Coefficients& old, const Coefficients& replacements)
{
    // note: the coefficients are pointers
    *old = *replacements;
}



//==============================================================================
// This creates and set the coefficients for the peak filter according to the
// parameters values from the chainSettings
void SimpleEQAudioProcessor::updatePeakFilter (const ChainSettings& chainSettings)
{
    // creates the coefficients
    auto peakCoefficients = makePeakFilter(chainSettings, getSampleRate());
    
    // setting the coefficients in both channels
    updateCoefficients(leftChain.get<ChainPositions::Peak>().coefficients, peakCoefficients);
    updateCoefficients(rightChain.get<ChainPositions::Peak>().coefficients, peakCoefficients);
    
}

//==============================================================================
template <int Index, typename ChainType, typename CoefficientType>
void update (ChainType& chain, const CoefficientType& coefficients)
{
    updateCoefficients(chain.template get<Index>().coefficients, coefficients[Index]);
    chain.template setBypassed<Index> (false);
}

// This function is used to update the cut filters (low cut and high cut)
template<typename ChainType, typename CoefficientType>
void updateCutFilter (ChainType& chain,
                      const CoefficientType& cutCoefficients,
                      const Slope& slope)
{
    
    chain.template setBypassed<0>(true);
    chain.template setBypassed<1>(true);
    chain.template setBypassed<2>(true);
    chain.template setBypassed<3>(true);
     
    switch (slope) {
     // NOTE: the cases don't have a break statement.
     // For example, if the slope is 36
     // then update<2>, update<1> and update<0> are called.
        case Slope_48:
        {
            update<3>(chain, cutCoefficients);
        }
            
        case Slope_36:
        {
            update<2>(chain, cutCoefficients);
        }
            
        case Slope_24:
        {
            update<1>(chain, cutCoefficients);
        }
            
        case Slope_12:
        {
            update<0>(chain, cutCoefficients);
        }
    }
    
}

//==============================================================================
// This creates and set the coefficients for the low cut filter according to the
// parameters values from the chainSettings
void SimpleEQAudioProcessor::updateLowCutFilters(const ChainSettings &chainSettings)
{
    auto& leftLowCut  = leftChain.get<ChainPositions::LowCut>();
    auto& rightLowCut = rightChain.get<ChainPositions::LowCut>();

    // create the coefficients for low cut filter
    auto lowCutCoefficients = makeLowCutFilter(chainSettings, getSampleRate());
    
    // assign the coefficients to the filter in both channels
    updateCutFilter (leftLowCut , lowCutCoefficients, chainSettings.lowCutSlope);
    updateCutFilter (rightLowCut, lowCutCoefficients, chainSettings.lowCutSlope);
    
}


//==============================================================================
// This creates and set the coefficients for the high cut filter according to the
// parameters values from the chainSettings
void SimpleEQAudioProcessor::updateHighCutFilters(const ChainSettings &chainSettings)
{
    auto& leftHighCut  = leftChain.get<ChainPositions::HighCut>();
    auto& rightHighCut = rightChain.get<ChainPositions::HighCut>();

    
    // create the coefficients for high cut filter
    auto highCutCoefficients = makeHighCutFilter(chainSettings, getSampleRate());
    
    updateCutFilter (leftHighCut,  highCutCoefficients, chainSettings.highCutSlope);
    updateCutFilter (rightHighCut, highCutCoefficients, chainSettings.highCutSlope);
    
}

//==============================================================================
// This updates all the filters (low cut, high cut, peak) using the parameters values
// from chainSettings
void SimpleEQAudioProcessor::updateFilters()
{
    auto chainSettings = getChainSettings(apvts);
    
    updateLowCutFilters(chainSettings);
    updatePeakFilter(chainSettings);
    updateHighCutFilters(chainSettings);
}


//==============================================================================

juce::AudioProcessorValueTreeState::ParameterLayout SimpleEQAudioProcessor:: createParameterLayout()
{
    juce::AudioProcessorValueTreeState::ParameterLayout layout;
    
    layout.add(std::make_unique<juce::AudioParameterFloat>("LowCut Freq",   //ID
                                                           "LowCut Freq",   //Name
                                                           juce::NormalisableRange<float> (20.f, 20000.f, 1.f, 0.25f), //Range
                                                           20.f)); // Default value
    
    layout.add(std::make_unique<juce::AudioParameterFloat>("HighCut Freq",
                                                           "HighCut Freq",
                                                           juce::NormalisableRange<float> (20.f, 20000.f, 1.f, 0.25f),
                                                            20000.f));
    
    
    layout.add(std::make_unique<juce::AudioParameterFloat>("Peak Freq",
                                                           "Peak Freq",
                                                           juce::NormalisableRange<float> (20.f, 20000.f, 1.f, 0.25f),
                                                            750.f));
    
    
    layout.add(std::make_unique<juce::AudioParameterFloat>("Peak Gain",
                                                           "Peak Gain",
                                                           juce::NormalisableRange<float> (-24.f, 24.f, 0.5f, 1.f),
                                                            0.f));
    
    
    layout.add(std::make_unique<juce::AudioParameterFloat>("Peak Q",
                                                           "Peak Q",
                                                           juce::NormalisableRange<float> (0.1f,10.f, 0.5f, 1.f),
                                                           1.f));
    
    juce::StringArray slopeChoices;
    
    for (int i = 0; i < 4; ++i) {
        // possibles slopes are -> 12dB/Oct,24dB/Oct,36dB/Oct,48dB/Oct
        juce::String str;
        str << 12 + i * 12;
        str << " db/Oct" ;
        
        slopeChoices.add(str);
    }
    
    layout.add (std::make_unique<juce::AudioParameterChoice> ("LowCut Slope",
                                                              "LowCut Slope",
                                                               slopeChoices,
                                                               0));
    
    layout.add (std::make_unique<juce::AudioParameterChoice> ("HighCut Slope",
                                                              "HighCut Slope",
                                                               slopeChoices,
                                                               0));
    
    return layout;
}
