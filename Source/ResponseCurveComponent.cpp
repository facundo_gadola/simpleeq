#include <JuceHeader.h>
#include "ResponseCurveComponent.h"


//==============================================================================
ResponseCurveComponent::ResponseCurveComponent (SimpleEQAudioProcessor &p) :
audioProcessor(p),
leftPathProducer(audioProcessor.leftChannelFifo),
rightPathProducer(audioProcessor.rightChannelFifo)


{
    // register as listener of all the audio parameters
    // so that when the parameters changes, the response curve is repainted
    const auto& parameters = audioProcessor.getParameters();
    for (auto param : parameters)
    {
        param->addListener(this);
    }
    
    
    updateChain();
    
    startTimerHz(60);
}

ResponseCurveComponent::~ResponseCurveComponent ()
{
    const auto& parameters = audioProcessor.getParameters();
    for (auto param : parameters)
    {
        param->removeListener(this);
    }
     
}

//==============================================================================
void ResponseCurveComponent::paint (juce::Graphics& g)
{
    g.fillAll (juce::Colour (19u,18u,42u));
    
    g.drawImage(background, getLocalBounds().toFloat());
    
    auto responseArea = getResponseCurveArea();
    
    // filters
    auto& lowCut  = monoChain.get<ChainPositions::LowCut>();
    auto& highCut = monoChain.get<ChainPositions::HighCut>();
    auto& peak    = monoChain.get<ChainPositions::Peak>();

    auto sampleRate = audioProcessor.getSampleRate();
    
    std::vector<double> magnitudes;
    
    /* The length of the magnitudes vector is equal to the width
       of the response area. So that each "pixel" corresponds to a frequency. */
    magnitudes.resize(responseArea.getWidth());
    
    for (int i= 0; i < responseArea.getWidth(); ++i)
    {
        double mag = 1.f;
        
        // map from normalized "pixel space" to "frequency space"
        auto freq = juce::mapToLog10(double(i)/double(responseArea.getWidth()), 20.0, 20000.0);
      
        
        // for each filter, obtain the magnitude for the current frequency
        // magnitude is expressed in gain, so mag is the multiplication of all the magnitudes.
        
        if (!monoChain.isBypassed<ChainPositions::Peak>())
            mag *= peak.coefficients->getMagnitudeForFrequency(freq, sampleRate);
        
        
        if (!lowCut.isBypassed<0>())
            mag *= lowCut.get<0>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
        if (!lowCut.isBypassed<1>())
            mag *= lowCut.get<1>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
        if (!lowCut.isBypassed<2>())
            mag *= lowCut.get<2>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
        if (!lowCut.isBypassed<3>())
            mag *= lowCut.get<3>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
        
        if (!highCut.isBypassed<0>())
            mag *= highCut.get<0>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
        if (!highCut.isBypassed<1>())
            mag *= highCut.get<1>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
        if (!highCut.isBypassed<2>())
            mag *= highCut.get<2>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
        if (!highCut.isBypassed<3>())
            mag *= highCut.get<3>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
        
        magnitudes[i] = juce::Decibels::gainToDecibels(mag);
    }
    
    juce::Path responseCurve;
    const double outputMin = responseArea.getBottom();
    const double outputMax = responseArea.getY();
    
    auto map = [outputMin, outputMax] (double input)
    {
        // map from the dB range to responseArea range
        // so that the max of 24dB is at the top of the response area
        return juce::jmap(input, -24.0, 24.0, outputMin, outputMax);
    };
    

    responseCurve.startNewSubPath(responseArea.getX(), map (magnitudes.front()));
    
    for (size_t i = 1; i < magnitudes.size(); ++i)
    {
        responseCurve.lineTo(responseArea.getX() + i, map(magnitudes[i]));
    }
    
    
    auto leftChannelFFTPath  = leftPathProducer.getPath();
    auto rightChannelFFTPath = rightPathProducer.getPath();

    
    leftChannelFFTPath.applyTransform(juce::AffineTransform().translation(responseArea.getX(),
                                                                          responseArea.getY()));
    rightChannelFFTPath.applyTransform(juce::AffineTransform().translation(responseArea.getX(),
                                                                          responseArea.getY()));
    // draw the spectrum analyzer path
    g.setColour(juce::Colours::blue);
    g.strokePath(leftChannelFFTPath, juce::PathStrokeType(1.f));
    g.setColour(juce::Colours::coral);
    g.strokePath(rightChannelFFTPath, juce::PathStrokeType(1.f));
    
    
    
    // draw the response curve
    g.setColour(juce::Colours::navajowhite);
    g.strokePath(responseCurve, juce::PathStrokeType(1.5f));
  
}

void ResponseCurveComponent::parameterValueChanged (int parameterIndex, float newValue)
{
    // This has to be fast. So the only thing that it do is set parametersChanged=true
    parametersChanged.set(true);
}

void ResponseCurveComponent::timerCallback()
{
    
    auto fftBounds  = getResponseCurveArea().toFloat();
    auto sampleRate = audioProcessor.getSampleRate();
    
    leftPathProducer.process(fftBounds, sampleRate);
    rightPathProducer.process(fftBounds, sampleRate);

    

    if (parametersChanged.compareAndSetBool(false, true))
    {
        // update chain only if some parameter changed
        updateChain();
    }
    
    repaint();
}

void ResponseCurveComponent::updateChain()
{
    auto sampleRate = audioProcessor.getSampleRate();
    
    // get parameters values from the value tree state
    auto chainSettings = getChainSettings(audioProcessor.apvts);
    
    // make the filters coefficients
    auto peakCoefficients = makePeakFilter(chainSettings, sampleRate);
    auto lowCutCoefficients  = makeLowCutFilter (chainSettings, sampleRate);
    auto highCutCoefficients = makeHighCutFilter(chainSettings, sampleRate);
    
    updateCoefficients(monoChain.get<ChainPositions::Peak>().coefficients, peakCoefficients);
    updateCutFilter(monoChain.get<ChainPositions::LowCut>(), lowCutCoefficients,chainSettings.lowCutSlope);
    updateCutFilter(monoChain.get<ChainPositions::HighCut>(), highCutCoefficients, chainSettings.highCutSlope);
}


//==============================================================================
void ResponseCurveComponent::resized()
{
    background = juce::Image(juce::Image::PixelFormat::RGB, getWidth(), getHeight(), true);
    
    juce::Graphics g (background);
    
    juce::Array<float> freqs
    {
        20,50,100,
        200,500,1000,
        2000,5000,10000,
        20000
    };
    
    auto renderArea = getResponseCurveArea();
    auto left = renderArea.getX();
    auto right = renderArea.getRight();
    auto top = renderArea.getY();
    auto bottom = renderArea.getBottom();
    auto width = renderArea.getWidth();
    
    juce::Array<float> xs;
    for (auto f: freqs)
    {
        auto normX = juce::mapFromLog10(f, 20.f, 20000.f);
        xs.add(left + width*normX);
    }
    
    g.setColour(juce::Colours::dimgrey);
    for (auto x: xs)
    {
        g.drawVerticalLine(x , top, bottom);
    }
    
    juce::Array<float> gains
    {
        -24, -12, 0, 12, 24
    };
    
    for (auto gain : gains) {
        auto y = juce::jmap(gain, -24.f, 24.f, float (bottom), float(top));
        
        if (gain == 0.f)
            g.setColour(juce::Colours::ghostwhite);
        else
            g.setColour(juce::Colours::dimgrey);
            
        g.drawHorizontalLine(y, left, right);
    }
    
    g.setColour(juce::Colours::lightgrey);
    const int fontHeight = 10;
    g.setFont(fontHeight);
    
    for (int i = 0; i < freqs.size(); ++i)
    {
        auto f = freqs[i];
        auto x = xs [i];
        
        bool greaterThan1000 = false;
        juce::String text;
        
        if (f > 999.f)
        {
            greaterThan1000 = true;
            f = f / 1000.f;
        }
        
        text << f;
        if (greaterThan1000)
            text << "k";
        
        text << "Hz";
        
        auto textWidth = g.getCurrentFont().getStringWidth(text);
        
        juce::Rectangle<int> textRect;
        textRect.setSize(textWidth, fontHeight);
        textRect.setCentre(x, 0);
        textRect.setY(1);
        
        g.drawFittedText(text, textRect, juce::Justification::centred, 1);
    }
    
    
    for (auto gain : gains) {
        auto y = juce::jmap(gain, -24.f, 24.f, float (bottom), float(top));
        
        juce::String text;
        if (gain > 0)
            text << "+";
        text << gain;
        
        auto textWidth = g.getCurrentFont().getStringWidth(text);
        
        juce::Rectangle<int> textRect;
        textRect.setSize(textWidth, fontHeight);
        textRect.setX(getWidth()-textWidth);
        textRect.setCentre(textRect.getCentreX(), y);
        
        if (gain == 0.f)
            g.setColour(juce::Colours::ghostwhite);
        else
            g.setColour(juce::Colours::lightgrey);
        
        g.drawFittedText(text, textRect, juce::Justification::centred, 1);
        
        
    }
    
    
}

juce::Rectangle<int> ResponseCurveComponent::getRenderArea()
{
    auto bounds = getLocalBounds();
    bounds.removeFromTop(12);
    bounds.removeFromBottom(2);
    bounds.removeFromLeft(22);
    bounds.removeFromRight(22);
    
    return bounds;
}

juce::Rectangle<int> ResponseCurveComponent::getResponseCurveArea()
{
    auto bounds = getRenderArea();
    bounds.removeFromTop(6);
    bounds.removeFromBottom(6);
    
    
    return bounds;
}


