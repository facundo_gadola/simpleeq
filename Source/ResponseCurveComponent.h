#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "Utils/FFT_data_generator.h"
#include "Utils/PathProducer.h"





//==============================================================================
class ResponseCurveComponent : public juce::Component,
                                      juce::AudioProcessorParameter::Listener,
                                      juce::Timer
{
public:
    
    ResponseCurveComponent (SimpleEQAudioProcessor&);
    ~ResponseCurveComponent() override;
    
    void parameterValueChanged (int parameterIndex, float newValue) override;
    void parameterGestureChanged (int parameterIndex, bool gestureIsStarting) override {};
    void timerCallback() override;
    void paint (juce::Graphics& g) override;
    void resized() override;
    
    
private:
    
    SimpleEQAudioProcessor& audioProcessor;
    juce::Atomic<bool> parametersChanged {false};
    
    // This processorChain is independent to the one used in the audioProcessor
    MonoChain monoChain;
    
    void updateChain();
    
    juce::Image background;
    
    juce::Rectangle<int> getRenderArea();
    juce::Rectangle<int> getResponseCurveArea();
    
    // This is for the spectrum analyzer
    PathProducer leftPathProducer, rightPathProducer;
    
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ResponseCurveComponent)

};
