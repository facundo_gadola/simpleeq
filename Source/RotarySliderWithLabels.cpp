#include <JuceHeader.h>
#include "RotarySliderWithLabels.h"





LookAndFeel::LookAndFeel ()
{
    // loads the image for the slider
    sliderImg = juce::ImageFileFormat::loadFrom(BinaryData::knobImg_png, BinaryData::knobImg_pngSize);
}


void LookAndFeel::drawRotarySlider(juce::Graphics& g, int x, int y, int width, int height,
                                   float sliderPosProportional, float rotaryStartAngle, float rotaryEndAngle, juce::Slider & slider)
{
    if (sliderImg.isValid())
    {
        // if the slider image is OK then display it
       
        auto sliderValue = slider.getValue();
        auto sliderMin   = slider.getMinimum();
        auto sliderMax   = slider.getMaximum();
        
        const double rotation = (sliderValue - sliderMin) / (sliderMax - sliderMin);

        const int frames = sliderImg.getHeight() / sliderImg.getWidth();
        const int frameId = int (ceil(rotation * (double(frames - 1.0))));
        
        const float radius = juce::jmin(width / 2.0f, height / 2.0f);
        const float centerX = x + width * 0.5f;
        const float centerY = y + height * 0.5f;
        
        const float rx = centerX - radius - 1.0f;
        const float ry = centerY - radius;
        
        g.drawImage(sliderImg,
                    int (rx),
                    int (ry),
                    2 * int (radius),
                    2 * int (radius),
                    0,
                    frameId*sliderImg.getWidth(),
                    sliderImg.getWidth(),
                    sliderImg.getWidth());
    
    }
    
    else {
        
        // if the slider image doesnt work, then show ellipses
        
        auto bounds = juce::Rectangle<float>(x,y,width,height);
        
        // draw the ellipse
        g.setColour(juce::Colours::rebeccapurple);
        g.fillEllipse(bounds);
        
        // draw borders of the ellipse
        g.setColour(juce::Colours::darkblue);
        g.drawEllipse(bounds, 0.8f);
        
        if (auto* rotarySlider = dynamic_cast<RotarySliderWithLabels*>(&slider))
        {
            auto center = bounds.getCentre();
            
            juce::Path path;
            
            juce::Rectangle<float> r;
            r.setLeft (center.getX() - 2);
            r.setRight(center.getX() + 2);
            r.setTop(bounds.getY());
            r.setBottom(center.getY() - rotarySlider->getTextHeight() * 1.5);
            
            path.addRoundedRectangle(r, 1.f);
            
            
            auto sliderAngleRad = juce::jmap(sliderPosProportional, 0.f, 1.f , rotaryStartAngle, rotaryEndAngle);
            
            path.applyTransform(juce::AffineTransform().rotated(sliderAngleRad, center.getX(), center.getY()));
            
            g.fillPath(path);
            
            g.setFont(rotarySlider->getTextHeight());
            auto text = rotarySlider->getDisplayString();
            auto strWidth = g.getCurrentFont().getStringWidth(text);
            
            r.setSize(strWidth + 4, rotarySlider->getTextHeight() + 2);
            r.setCentre(bounds.getCentre());
            
            g.setColour(juce::Colours::transparentBlack);
            g.fillRect(r);
            
            g.setColour(juce::Colours::lightgrey);
            g.drawFittedText(text, r.toNearestInt(), juce::Justification::centred, 1);
        }
    }
}



//==============================================================================
void RotarySliderWithLabels::paint(juce::Graphics& g)
{
    auto startAngle = juce::degreesToRadians(180.f + 45.f);
    auto endAngle   = juce::degreesToRadians(180.f -  45.f) + juce::MathConstants<float>::twoPi;
    
    auto range = getRange();
    auto sliderBounds = getSliderBounds();
    
    
    
    getLookAndFeel().drawRotarySlider(g ,
                                      sliderBounds.getX(),
                                      sliderBounds.getY() ,
                                      sliderBounds.getWidth(),
                                      sliderBounds.getHeight(),
                                      juce::jmap(getValue(), range.getStart(), range.getEnd(), 0.0, 1.0 ), startAngle,
                                      endAngle,
                                      *this);
    
    
    auto center = sliderBounds.toFloat().getCentre();
    auto radius = sliderBounds.getWidth()/2;
    
    
    // draw the (units) labels below each slider
    g.setColour(juce::Colours::lightgrey);
    g.setFont(getTextHeight());
    
    for (int i = 0; i < labels.size(); ++i)
    {
        auto pos = labels[i].position;
        auto angle = juce::jmap(pos, 0.f, 1.f, startAngle, endAngle);
        
        auto c = center.getPointOnCircumference(radius + getTextHeight()/2 + 1, angle);
        
        juce::Rectangle<float> rect;
        auto str = labels[i].label;
        
        rect.setSize(g.getCurrentFont().getStringWidth(str), getTextHeight());
        rect.setCentre(c);
        rect.setY(rect.getY() + getTextHeight());
        
        g.drawFittedText(str, rect.toNearestInt(), juce::Justification::centred, 1);
        
    }
    
}

juce::Rectangle<int> RotarySliderWithLabels::getSliderBounds() const
{
    auto bounds = getLocalBounds();
    auto size = juce::jmin(bounds.getWidth(), bounds.getHeight());
    
    size = size - getTextHeight() * 2;
    
    juce::Rectangle<int> rect;
    rect.setSize(size, size);
    rect.setCentre(bounds.getCentreX(),0);
    rect.setY(2);
    
    return rect;
}


juce::String RotarySliderWithLabels::getDisplayString() const
{
    // This function returns the string to display for the value of the slider.
    
    if (auto* choiceParam = dynamic_cast<juce::AudioParameterChoice*>(parameter))
    {
        return choiceParam->getCurrentChoiceName();
    }
    
    juce::String str;
    bool greaterThan1000 = false;  // if the frequency is greater than 1000 -> display as Khz
    
    if (auto* floatParam = dynamic_cast<juce::AudioParameterFloat*>(parameter))
    {
        float value = getValue();
        if (value > 999.f)
        {
            value = value / 1000.f;
            greaterThan1000 = true;
        }
        
        str = juce::String(value, (greaterThan1000 ? 2 : 0));
    }
    
    if (unitSuffix.isNotEmpty())
    {
        str << " ";
        if (greaterThan1000)
            str << "k";
        
        str << unitSuffix;
    }
    
    return str;
    
}


