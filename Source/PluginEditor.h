
#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "RotarySliderWithLabels.h"
#include "ResponseCurveComponent.h"




//==============================================================================
class SimpleEQAudioProcessorEditor  : public juce::AudioProcessorEditor
                                       
{
public:
    SimpleEQAudioProcessorEditor (SimpleEQAudioProcessor&);
    ~SimpleEQAudioProcessorEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;
    

private:

    SimpleEQAudioProcessor& audioProcessor;
    
    //==============================================================================
    // Sliders
    RotarySliderWithLabels peakFreqSlider     {*audioProcessor.apvts.getParameter("Peak Freq"),   "Hz" };
    RotarySliderWithLabels peakGainSlider     {*audioProcessor.apvts.getParameter("Peak Gain"),   "dB " };
    RotarySliderWithLabels peakQSlider        {*audioProcessor.apvts.getParameter("Peak Q"),       "" };
    RotarySliderWithLabels lowCutFreqSlider   {*audioProcessor.apvts.getParameter("LowCut Freq"),  "Hz" };
    RotarySliderWithLabels highCutFreqSlider  {*audioProcessor.apvts.getParameter("HighCut Freq"), "Hz" };
    RotarySliderWithLabels lowCutSlopeSlider  {*audioProcessor.apvts.getParameter("LowCut Slope"), "dB/Oct" };
    RotarySliderWithLabels highCutSlopeSlider {*audioProcessor.apvts.getParameter("HighCut Slope"),"dB/Oct" };
    
    
    ResponseCurveComponent responseCurveComponent {audioProcessor};
    
    
    using Attachment = juce::AudioProcessorValueTreeState::SliderAttachment;
    
    //==============================================================================
    // Attachments between parameters and sliders
    Attachment peakFreqSliderAttachment     {audioProcessor.apvts, "Peak Freq",    peakFreqSlider};
    Attachment peakGainSliderAttachment     {audioProcessor.apvts, "Peak Gain",    peakGainSlider};
    Attachment peakQSliderAttachment        {audioProcessor.apvts, "Peak Q",       peakQSlider};
    Attachment lowCutFreqSliderAttachment   {audioProcessor.apvts, "LowCut Freq",  lowCutFreqSlider};
    Attachment highCutFreqSliderAttachment  {audioProcessor.apvts, "HighCut Freq", highCutFreqSlider};
    Attachment lowCutSlopeSliderAttachment  {audioProcessor.apvts, "LowCut Slope", lowCutSlopeSlider};
    Attachment highCutSlopeSliderAttachment {audioProcessor.apvts, "HighCut Slope",highCutSlopeSlider};

    
    // This rectangles are used to display the filter's labels
    juce::Rectangle<int> lowCutLabelsArea;
    juce::Rectangle<int> peakLabelsArea;
    juce::Rectangle<int> highCutLabelsArea;

    
    // This function returns a vector of the GUI components
    std::vector<juce::Component*> getComponents();
    

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SimpleEQAudioProcessorEditor)
};
