#pragma once

#include <JuceHeader.h>



//==============================================================================
// Custom look and feel for the rotary slider
class LookAndFeel : public juce::LookAndFeel_V4
{
public:
    
    LookAndFeel();
    
    void drawRotarySlider (juce::Graphics&, int x, int y, int width, int height,
                                      float sliderPosProportional,
                                      float rotaryStartAngle,
                                      float rotaryEndAngle,
                                      juce::Slider& slider) override;
private:
    
    juce::Image sliderImg;
        
};


//==============================================================================
class RotarySliderWithLabels : public juce::Slider
{
public:
    RotarySliderWithLabels (juce::RangedAudioParameter& rangedAudioParam, const juce::String& unit) :                               juce::Slider (juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag,
                              juce::Slider::TextEntryBoxPosition::NoTextBox),
                              parameter(&rangedAudioParam),
                              unitSuffix (unit)
    {
        setLookAndFeel(&customLookAndFeel);
    }
    
    ~RotarySliderWithLabels ()
    {
        setLookAndFeel (nullptr);
    }
    
    struct LabelPos
    {
        float position;      // normalized position where the label should be
        juce::String label;  // label of the slider at that position
    };
    
    juce::Array<LabelPos> labels;
    
    void paint (juce::Graphics& g) override;
    juce::Rectangle<int> getSliderBounds() const;
    int getTextHeight() const { return 14; }
    juce::String getDisplayString () const;
    
    
private:
    
    LookAndFeel customLookAndFeel;
    
    juce::RangedAudioParameter* parameter;
    
    // This is the unit of the slider value (for example 'hz' in case of freq)
    juce::String unitSuffix;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RotarySliderWithLabels)

};
